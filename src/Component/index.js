import React from 'react'
import LoginPanel from './LoginPanelComponent'
import ForgotPIN from './ForgotPINPanelComponent'
import Dashboard from './DashboardComponent'
import NotFound from './NotFound'
import {Switch, Route} from 'react-router-dom'

class App extends React.Component{
    render(){
        return  <Switch>
                        <Route exact path="/" component={LoginPanel}/>
                        <Route path="/ForgotPIN" component={ForgotPIN}/>
                        <Route path="/Dashboard" component={Dashboard}/>
                        <Route component={NotFound}/>
                     </Switch>
    }
}

export default App;