import React from 'react'
import './index.css'

function NotFound(props){
    function Goback (){
        window.history.back(-1);
    }
    return <div className="NotFoundPage GeneralPanel">
                    <p>Resource is Not Found...</p>
                    <button onClick={Goback} className="GeneralButtonType1">Go Back</button>
                </div>
}

export default NotFound