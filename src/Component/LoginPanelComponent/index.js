import React from 'react'
import './index.css'
import LOGO from '../../Media/Images/Logo.png'
import validator from 'validator'
import {Link} from 'react-router-dom'
import requestPromise from 'request-promise';
import {developmentServerURL} from '../../configure'
class LoginPanel extends React.Component{
    constructor(props){
        super(props);
        this.emailReference = React.createRef();
        this.passwordReference = React.createRef();
        this.state={
            emailInvalid: false,
            passwordEmpty: false,
            loginFailed: false
        }
    }
    componentDidMount(){
        let URL = process.env.NODE_ENV==='development'?developmentServerURL+"users/check_auth":"users/check_auth";
        let options = { method: 'GET',  url: URL, withCredentials: true}
        requestPromise(options)
        .then(response=>{
            this.props.history.push('/Dashboard');
            return null;
        }).catch(error=>{ console.log("Error: ", error.message); })
    }
    LoginPreCheck = () =>{ 
        let emailResult = validator.isEmail(this.emailReference.current.value);
        let passwordResult = validator.isEmpty(this.passwordReference.current.value);
        this.setState({ emailInvalid: !emailResult,  passwordEmpty: passwordResult},()=>{
            if(this.state.emailInvalid===false && this.state.passwordEmpty=== false)
                this.Login();
        });
    }
    Login = () =>{
        let requestBody={
            username: this.emailReference.current.value,
            password: this.passwordReference.current.value
        }
        let URL = process.env.NODE_ENV==='development'?developmentServerURL+"login":"login";
        let options = { method: 'POST',  url: URL, body: requestBody, json: true, withCredentials: true}
        requestPromise(options)
        .then(response=>{
            this.props.history.push('/Dashboard');
            return null;
        }).catch(error=>{ 
            console.log("Error: ", error.message);
            this.setState({ loginFailed: true });
        })
    }
    render(){
        let checkResult = null;            
        if(this.state.emailInvalid) checkResult= <p>Invalid Email...</p>
        else if(this.state.passwordEmpty) checkResult = <p>Need Password...</p>
        else if(this.state.loginFailed) checkResult = <p>Authentication Failed...</p>
        return  <div id="LoginPanel" className="GeneralPanel">
                        <img className="CompanyLOGO" src={LOGO} alt=""></img>
                        <p className="Welcome">Welcome Back to Phaze</p>
                        <div className="UserInput">
                            <p className="Fontawesome-Icon"><i className="fas fa-envelope"></i></p>
                            <input type="email" placeholder="Your Email..."  ref={this.emailReference}/>
                        </div>
                        <div className="UserInput">
                            <p className="Fontawesome-Icon"><i className="fas fa-key"></i></p>
                            <input type="password" placeholder="Your Password..."  ref={this.passwordReference}/>
                        </div>
                        <div className="forgotPassword">
                            <Link to='/ForgotPIN'>FORGOT PASSWORD?</Link>
                        </div>
                        <div className="ErrorNotice">
                            {checkResult}
                        </div>
                        <button onClick={this.LoginPreCheck} className="GeneralButtonType1">LOG IN</button>
                        <p className="ShiftToSignUp">
                            ARE YOU NEW HERE? <span>SIGN UP</span>!
                        </p>
                    </div>
    }
}

export default LoginPanel;