import React from 'react'
import './index.css'
import requestPromise from 'request-promise';
import {developmentServerURL} from '../../configure'
import Avatar from '../../Media/Images/Avatar.png'
class DashBoard extends React.Component{
    constructor(props){
        super(props);
        this.menuReference = React.createRef();
        this.state={
            autoCheckLoginStatus: true,
            menuShow: false
        }
    }
    componentDidMount(){
        let URL = process.env.NODE_ENV==='development'? developmentServerURL+"users/check_auth":"users/check_auth";
        let options = { method: 'GET', url: URL, withCredentials: true
        }
        requestPromise(options).then(response=>null).catch(error=>{ 
            console.log("Error: ", error.message);
            this.props.history.push('/');
        })
    }
    MenuClicked = () =>{
        this.setState(prevState=>{
            return { menuShow: !prevState.menuShow}
        })
    }
    render(){
        let MenuOptionClass = this.state.menuShow?"Active":"";
        let MenuClass = this.state.menuShow? "Show":"Hide";
        return  <div id="DashBoardPanel" className="GeneralPanel">
                        <div className="DashboardBar">
                            <p className={"Menu "+MenuOptionClass}  onClick={this.MenuClicked}><i className="fas fa-chevron-circle-right"></i></p>
                            <p className="DashboardTitle">Dashboard</p>
                            <img src={Avatar} alt="" />
                        </div>
                        <div className={"DashboardMenu "+MenuClass} ref={this.menuReference}>
                            <a className="DashboardMenu-Item">Account Setting</a>
                            <a className="DashboardMenu-Item">Transaction History</a>
                            <a className="DashboardMenu-Item">Log Out</a>
                        </div>
                     </div>
    }
}

export default DashBoard