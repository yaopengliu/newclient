import React from 'react'
import './index.css'
import LOGO from '../../Media/Images/Logo.png'
import validator from 'validator'
import {Link} from 'react-router-dom'
class ForgotPIN extends React.Component{
    constructor(props){
        super(props);
        this.emailReference = React.createRef();
        this.state={
            sent: false,
            emailInvalid: false
        }
    }
    componentDidMount(){
        
    }
    emailCheck = () =>{
        let result = validator.isEmail(this.emailReference.current.value);
        if(result===false)
            this.setState({ emailInvalid: true });
        else
            this.setState({ emailInvalid: false }, this.GetResetCode);
    }
    GetResetCode = () =>{
        console.log("try to get reset code...");
    }
    render(){
        return  <div id="ForgotPINPanel" className="GeneralPanel">
                        <img className="CompanyLOGO" src={LOGO} alt=""></img>
                        <p className="PasswordReset">Reset Your Password</p>
                        <div className="UserInput">
                            <p className="Fontawesome-Icon"><i className="fas fa-envelope"></i></p>
                            <input type="email" placeholder="Email for Reset..."  ref={this.emailReference}/>
                        </div>
                        <div className="ErrorNotice">
                            {this.state.emailInvalid?<p>Invalid Email...</p>:null}
                        </div>
                        <button onClick={this.emailCheck} className="GeneralButtonType1">GET RESET CODE</button>
                        <p className="divider">
                            ----------------------- <span>OR</span> -----------------------
                        </p>
                        <Link to='/' className="GeneralButtonType2">BACK TO LOGIN</Link>
                     </div>
    }
}

export default ForgotPIN;