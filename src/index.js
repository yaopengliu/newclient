import React from 'react'
import ReactDOM from 'react-dom'
import './bootstrap-reboot.min.css'
import './index.css'
import App from './Component'
import {BrowserRouter} from 'react-router-dom'
/**
 * ---------------------        Global Setting      -------------------
 */


ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>
, 
document.getElementById('root'));

